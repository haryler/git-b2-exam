# Partiel Versionning Git - ESGI2- S1 - Yannick COUSIN

## Etape 1 - Créer un dépôt Gitlab

![Screenshot dépot crée](screenshot1.png)

Le dépôt est bien créé et cloné sur le PC.
Mon README servira de fichier de rendu !

## Etape 2 - Créer une première documentation

J'ai apparemment anticipé l'étape 2 sans avoir créer de branche !
Voici donc quand même la création d'une branche documentation/commands ainsi que le descript de 6 commandes dans le fichier "commandes.md"

![Screenshot branch documentation/commands](screenshot2.png)


La merge request :

![Screenshot merge request](screenshot3.png)


## Etape 3 - Créer une deuxième documentation

Créons la nouvelle branche documentation/gitlab.

![Screenshot Step 3](screenshot4.png)


### Comment protéger une branche dans gitlab 


Settings > Repository > Protected branches

Ici on peut choisir qui est autorisé à merge et à push.

### Pourquoi protéger des branches dans git ?

On protège les branches car certaines branches sont automatiquement intégrées et déployées.
On evite de push sur ces branches pour vérifier la cohérence et la fonctionnalité du code d'abord.

### Qu'est-ce qui peut entraîner des merge conflicts et comment les résoudres

Si 2 développeurs modifient un même fichier, et plus particulièrement la même portion du fichier, chacu>
Il faut alors choisir la version que l'on souhaite conserver pour résoudre le conflit et pouvoir réalis>



## Etape 4 - Résolution de merge conflicts

Validons la 2eme merge request :

![Screenshot merge request 2](screenshot5.png)

Pas de conflit

Mais pour la 1ere merge request : 

![Screenshot conflit](screenshot6.png)

On doit le résoudre ! 
On va donc fetch ce qui manque suite au premier merge. Puis pull (en précisant que le pull doit merge)
On peut ensuite éditer le fichier pour garder la bonne version puis add, commit et push le changement.


![Screenshot resolve conflict1](screenshot7.png)

![Screenshot resolve conflict2](screenshot8.png)


## Etape Bonus - dépot multi origine

Le nouveau repo github est créé !

J'ajoute ce remote au repo (comme j'ai lu le sujet trop vite, je l'ai nommé origin2 au lieu de backup)

![screenshot add remote](screenshot9.png)


Malheureusement, les commandes données dans l'énnoncé me retourne des erreurs que je n'ai pas le temps de résoudre durant cet épreuve !
A refaire à la maison !

![screenshot fail](screenshot10.png)


