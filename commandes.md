# Etape 2 - description de 6 commandes : 

## git add

Cette commande met à jour l’index en utilisant le contenu actuel trouvé dans l’arbre de travail, pour préparer le contenu de la prochaine validation. Typiquement, elle ajoute intégralement le contenu actuel des chemins existants, mais peut aussi n’ajouter que certaines parties des modifications au moyen d’options ou soustraire certains chemins qui n’existent plus dans l’arbre de travail.

## git commit

Crée un nouveau commit contenant le contenu actuel de l’index et avec le message de validation décrivant la modification. Le nouveau commit est un fils direct de HEAD, habituellement au sommet de la branche actuelle et la branche est mise à jour pour pointer dessus.

## git push

Met à jour les références distantes à l’aide des références locales, tout en envoyant les objets nécessaires pour compléter les références données.

## git clone

Clone un dépôt dans un répertoire nouvellement créé, crée une branche de suivi à distance pour chaque branche du dépôt cloné.

## git pull

Intègre les modifications d’un dépôt distant dans la branche actuelle. Dans son mode par défaut, git pull est l’abréviation de git fetch suivi de git merge FETCH_HEAD.

## git init

Cette commande crée un dépôt Git vide - essentiellement un répertoire .git avec des sous-répertoires pour les fichiers objects, refs/heads, refs/tags et les fichiers de modèle. Un fichier HEAD initial qui fait référence à la tête de la branche principale (master) est également créé.



